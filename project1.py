import numpy as np
import scipy.stats as stats
import scipy.optimize as optimize
from scipy.stats import norm
import scipy as sp
import cv2
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from skimage import transform
import cmath


def import_image(image_names):
	# imports images in greyscale
	images = {}

	for i, image in enumerate(image_names): 

		images[i] = cv2.imread(image, 0)

		if images[i] == None:
			print "Error reading images!"
			print i
			exit()

	return images


def preprocess(img, rescale=False, convolve=False):
	# rescales pixel intensity to a 32-value scale
	if rescale: 
		print "Rescaling..."
		img = (img * (1.0/255)) * 32

	# convolves using a Sobel filter in the x direction
	if convolve: 
		print "Convolving..."
		kernel = np.array([-1, 0, 1])
		img = cv2.filter2D(img, -1, kernel)
		# image = cv2.Sobel(image, -1, 1, 0, ksize=3)

	return img


def create_histogram(img, title, random=False, downsized=False):
	xmin = -31
	xmax = 31
	step = 1

	# Calculations
	flat_img = img.flatten()

	# returns values of the histogram, bin_edges
	y, x = np.histogram(img, density=True, bins=np.linspace(xmin, xmax, (xmax - xmin) + 1))

	bin_centers = (x[:-1] + x[1:])/2

	hist_fit, hist_stats = gen_gaussians(img, bin_centers, x, y, random)

	nbins = y.size


	# Plotting
	plt.figure(1)

	mean, var, kurtosis = get_statistics(img)


	# Regular plots
	plt.subplot(121)
	plt.title("H(z)")
	plt.xlabel("Intensity")	
	plt.hist(flat_img, color='g', bins=nbins, normed=True, alpha=0.5)
	if not random and not downsized: 
		plt.plot(bin_centers, hist_fit, color="r", label="Fitted GGD")
	plt.plot(bin_centers, hist_stats, color="b", label="GGD w/params")
	plt.xlim(xmin + 1, xmax + 1)
	plt.legend()
	plt.grid(True)


	# Log plots
	plt.subplot(122)
	plt.title("Log H(z)")
	plt.xlabel("Intensity")	
	plt.plot(bin_centers, np.log(y), color="g", label="log(H(z))")
	plt.plot(bin_centers, np.log(hist_stats), color="b", label="Log GGD w/params")
	if not random and not downsized: 
		plt.plot(bin_centers, np.log(hist_fit), color="r", label="Log Fitted GGD")	
	plt.xlim(xmin - 1, xmax + 1)
	plt.legend(loc=3)
	plt.grid(True)

	plt.suptitle(title)
	plt.show()


def gen_gaussians(img, bin_centers, x, y, random=False):
	mean, var, kurtosis = get_statistics(img)
	sigma = np.sqrt(var)	

	if random: 
		coeffs = [1, 0, 0]
		hist_fit = None
	else: 
		coeffs, var_matrix = optimize.curve_fit(gauss, bin_centers, y, p0=[1.0, mean, sigma])
		hist_fit = gauss(bin_centers, *coeffs)

	hist_stats = gauss(bin_centers, coeffs[0], mean, sigma)

	return hist_fit, hist_stats	


def get_statistics(img):
	mean = np.mean(img)
	variance = np.var(img)
	kurtosis = np.mean(stats.kurtosis(img))

	return mean, variance, kurtosis


def gauss(x, *p):
    area, mu, sigma = p
    return area*np.exp(-(x-mu)**2/(2.*sigma**2))


def downsample(img):
	# Take average of every 2x2 block
	return transform.downscale_local_mean(img, (2,2))


def noise_image():
	img = np.random.random_integers(0, 31, (256, 256)).astype(float)
	return img


def get_band_mask(spectrum, band, n_bands):
    """Select a circular frequency band from the spectrum"""
    # Get image coordinates, and center on 0
    x,y = np.meshgrid(range(spectrum.shape[1]),range(spectrum.shape[0]))
    x = x - np.max(x)/2
    y = y - np.max(y)/2
    
    # Compute distances from center
    radius = np.hypot(x,y)
    
    # Compute the min and max frequencies of this band
    bw = np.amin(spectrum.shape)/(n_bands*2)
    freqs = [0+bw*band,bw+bw*band]
    
    # Create the corresponding mask
    msk = np.zeros(spectrum.shape, dtype=bool)
    msk[(radius<freqs[1])*(radius>freqs[0])]=True
    
    # Do not include the zero-th frequency (overall luminance)
    msk[x.shape[0]/2,y.shape[0]/2] = False
    return msk


def get_rotational_average(A, n_bands):
    """Average over the contents of n frequency bands"""
    res = []
    for band in np.arange(n_bands):
        msk = get_band_mask(A, band, n_bands)
        res.append(np.mean(A[msk].flatten())/A.size)
    return np.array(res, dtype=float)


def get_accum_power(A, n_bands):
	res = []

	for band in np.arange(n_bands):
		msk = get_band_mask(A, band, n_bands)
		res.append(np.sum(A[msk].flatten()))
	return np.array(res, dtype=float)


# From the DFT data, the accumulated power (AP) was obtained as a product of the increment of the frequency (=0.95 Hz) and the sum of power spectral densities over a frequency range between 1.9 and 7.6 Hz, which represents (fluctuations amplitude)2 within this frequency range.19)
def fourier(img):
	img_float32 = np.float32(img)
	img_fft = np.fft.fft2(img_float32)
	img_fft = np.fft.fftshift(img_fft)
	return img_fft


def logAf_vsf(img_fft, img_num):
	amp = np.abs(img_fft)**2
	bands = amp.shape[0] * 1/5

	rotavg = get_rotational_average(amp, bands)

	plt.plot(np.log(range(1, len(rotavg))), np.log(rotavg[1:]) + img_num, label="Image %d" % (i + 1))

	return rotavg


def integration(img_fft, img_num):
	amp2 = np.abs(img_fft)**2

	bands = amp2.shape[0] * 1/5

	power_sum = get_rotational_average(amp2, bands)

	n = img_fft.size
	freqs = np.fft.fftfreq(n)
	dx = freqs[1]

	slopes = np.diff(power_sum) * np.abs(dx)

	plt.plot(slopes, label="Image %d" % (img_num + 1))
	return dx, power_sum


class Line(object):
	def __init__(self, img_size):
		self.center = None
		self.orient = None
		self.length = None

	def endpoints(self, img_size):
		if self.length < 1: 
			return None, None

		half_len = int(self.length*0.5)

		delta_x = int(half_len*np.sin(self.orient))
		delta_y = int(half_len*np.cos(self.orient))

		start = [self.center[0] + delta_x, self.center[1] + delta_y]
		end = [self.center[0] - delta_x, self.center[1] - delta_y]

		start = (start[0], start[1])
		end = (end[0], end[1])

		return start, end


def generate_img(img_size, N):
	line_list = []

	def truncated_power_law(a, m):
	    x = np.arange(1, m+1, dtype='float')
	    pmf = 1/(x**a)
	    pmf /= pmf.sum()
	    return stats.rv_discrete(values=(range(1, m+1), pmf))

	a, m = 3, 1024
	d = truncated_power_law(a=a, m=m)

	for i in range(N):
		line = Line(img_size)
		line.orient = np.random.rand()*2*np.pi
		line.length = d.rvs(size=1)

		line_list.append(line)

	return line_list


def shorten_lines(lines, factor):
	for line in lines: 
		line.length *= factor


def make_image(lines, img_size):
	canvas = np.zeros((img_size,img_size))

	for line in lines: 
		line.center = (np.random.rand(1,2)*img_size).flatten().astype(int)		
		x, y = line.endpoints(img_size)
		
		if x == None: 
			continue
	
		cv2.line(canvas, x, y, thickness=1, color=255)

	return canvas


def choose_random_square(img, size):
	px = 128

	origin = (np.random.rand(1,2)*(size-px)).flatten().astype(int)

	return img[origin[1]:origin[1] + px, origin[0]:origin[0] + px]


def print_randStats(rand1, rand2):
	print "Rand 1: "
	print get_statistics(rand1)
	print "Rand 2: "
	print get_statistics(rand2)


if __name__ == "__main__":
	image_names = ['natural_scene_%d.jpg' % i for i in range(1,6)]

	images = import_image(image_names)

	# ### PROBLEM 1 ###
	img = preprocess(images[0], True, True)
	# create_histogram(img, "Natural Scene 1")
	print "\nnatural"
	print get_statistics(img)
	
	img_downsampled = downsample(img)
	print "\ndownsampled"
	print get_statistics(img_downsampled)
	create_histogram(img_downsampled, "Natural Scene 1, Downsampled", downsized=True)

	noise_img = noise_image()
	noise_img = preprocess(noise_img, False, True)
	print "\nrandom"
	print get_statistics(noise_img)

	create_histogram(noise_img, "Random Image", random=False)


	# ### PROBLEM 2 ###
	fft_orig = {}
	plt.figure()
	plt.subplot(121)
	plt.ylabel("log A(f)")
	plt.xlabel("log f")
	for i,img in enumerate(image_names):
		fft_orig[i] = fourier(images_downsampled[i])
		psd = logAf_vsf(fft_orig[i], i)	
	plt.title("Log A(f) against log f")
	plt.legend()

	plt.subplot(122)
	plt.ylabel("S(f0)")
	plt.xlabel("f0")
	for i,img in enumerate(image_names):
		integration(fft_orig[i], i)
	plt.title("S(f0) over f0")
	plt.legend()

	plt.suptitle("Natural Scene")
	plt.show()

	images_downsampled = {}
	fft_downsampled = {}
	for i, img in enumerate(image_names):
		images_downsampled[i] = downsample(images[i])

	plt.figure()
	plt.subplot(121)
	plt.ylabel("log A(f)")
	plt.xlabel("log f")
	for i,img in enumerate(image_names):
		fft_downsampled[i] = fourier(images_downsampled[i])
		psd = logAf_vsf(fft_downsampled[i], i)
	plt.title("Log A(f) against f")
	plt.legend()

	plt.subplot(122)
	plt.ylabel("S(f0)")
	plt.xlabel("f0")
	for i,img in enumerate(image_names):
		dx, power_sum = integration(fft_downsampled[i], i)
	plt.title("S(f0) over f0")
	plt.legend()

	plt.suptitle("Downsampled Natural Scene")
	plt.show()	


	# ### PROBLEM 3 ###

	line_list = generate_img(1024, 100000)
	canvas = make_image(line_list, 1024)
	rand1 = choose_random_square(canvas, 1024)
	rand2 = choose_random_square(canvas, 1024)
	plt.imshow(canvas)
	plt.subplot(231)
	plt.title("Cropped 1024x1024 image, 1")
	plt.imshow(rand1)
	plt.subplot(234)
	plt.title("Cropped 1024x1024 image, 2")	
	plt.imshow(rand2)
	plt.show()

	print_randStats(rand1, rand2)

	shorten_lines(line_list, 0.5)
	canvas = make_image(line_list, 512)
	rand1 = choose_random_square(canvas, 512)
	rand2 = choose_random_square(canvas, 512)
	plt.subplot(232)
	plt.title("Cropped 512x512 image, 1")
	plt.imshow(rand1)
	plt.subplot(235)
	plt.title("Cropped 512x512 image, 2")
	plt.imshow(rand2)
	plt.show()

	print_randStats(rand1, rand2)	

	shorten_lines(line_list, 0.5)
	canvas = make_image(line_list, 256)
	rand1 = choose_random_square(canvas, 256)
	rand2 = choose_random_square(canvas, 256)
	plt.subplot(233)
	plt.title("Cropped 256x256 image, 1")
	plt.imshow(rand1)
	plt.subplot(236)
	plt.title("Cropped 256x256 image, 2")
	plt.imshow(rand2)

	print_randStats(rand1, rand2)	

	plt.show()	




